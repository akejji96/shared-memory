
/* Deux fonctions a implementer: dsminit et dsmfileline
-- /!\ quand le processus distant execute dsminit après le dsmwrap.
Donc quand il est en train d'echanger il y a un bout d'information qui se fait
dans le dsmwrap et l'autre dans le dsminit. Ce n'est pas le meme contexte d'echange
mais c'est le meme processus donc ca marche.

-- mmap de la memoire partagee

-- tout le systeme repose sur les segmentations fault quand on essaye d'acceder a une page
dont on est pas proprietaire. Donc on met en place un traitant de signal. Pour recuperer
l'adresse qui a fait l'erreur on utilise un traitant etendu (segvhandler)

-- on fait un thread et on utilise dsm_comm_daemon pour y repondre

Differentes fonctions:

-- obtenir base_addr, l'adresse de la zone qui est partagee par tout le monde

-- finalisation: on ferme tout bien, (pthread_cancel a enlever)

-- a partir du numero de page, on obtient l'adresse de la page

-- change info permet de modifier les proprietés de la page (proprietaire, ...)

-- allocation d'une nouvelle page, on fait un mmap de type private (alloc page)

-- une fonction qui permet de lire/ecrire dedans

-- fonction qui libere la page d'adressage

-- a partir d'une adresse, recuperer le numero de la page */

#include "dsm.h"
#include "common_impl.h"
int DSM_NODE_NUM; /* nombre de processus dsm */
int DSM_NODE_ID;  /* rang (= numero) du processus */

/* indique l'adresse de debut de la page de numero numpage */
static char *num2address( int numpage )
{
  char *pointer = (char *)(BASE_ADDR+(numpage*(PAGE_SIZE)));

  if( pointer >= (char *)TOP_ADDR ){
    fprintf(stderr,"[%i] Invalid address !\n", DSM_NODE_ID);
    return NULL;
  }
  else return pointer;
}


int address2num ( char* addr){
  return (((long int)(addr-BASE_ADDR)))/(PAGE_SIZE);
}

/* fonctions pouvant etre utiles */

/* Modifier le status et proprietaire de la page */
static void dsm_change_info( int numpage, dsm_page_state_t state, dsm_page_owner_t owner)
{
  if ((numpage >= 0) && (numpage < PAGE_NUMBER)) {
    if (state != NO_CHANGE )
    table_page[numpage].status = state;
    if (owner >= 0 )
    table_page[numpage].owner = owner;
    return;
  }
  else {
    fprintf(stderr,"[%i] Invalid page number !\n", DSM_NODE_ID);
    return;
  }
}

/* Recuperer le proprietaire de la page */
static dsm_page_owner_t get_owner( int numpage)
{
  return table_page[numpage].owner;
}

/* Recuperer le status de la page */
static dsm_page_state_t get_status( int numpage)
{
  return table_page[numpage].status;
}

/* Allocation d'une nouvelle page */
static void dsm_alloc_page( int numpage )
{
  char *page_addr = num2address( numpage );
  mmap(page_addr, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  return ;
}

/* Changement de la protection d'une page */
static void dsm_protect_page( int numpage , int prot)
{
  char *page_addr = num2address( numpage );
  mprotect(page_addr, PAGE_SIZE, prot);
  return;
}

/* Désallocation d'une page */
static void dsm_free_page( int numpage )
{
  char *page_addr = num2address( numpage );
  munmap(page_addr, PAGE_SIZE);
  return;
}

static void *dsm_comm_daemon( void *arg)
{
  while(1)
  {
    /* a modifier */
    printf("[%i] Waiting for incoming reqs \n", DSM_NODE_ID);
    sleep(2);
  }
  return;
}
static void *dsm_accept_thread( void *arg)
{
  while(1){

  }
  return;
}
static void *dsm_connect_thread( void *arg)
{
  while(1){

  }
  return;
}
/* on envoie la page */
static int dsm_send(int dest,void *buf,size_t size)
{
  /* a completer */
}

/* on recoit la page */
static int dsm_recv(int from,void *buf,size_t size)
{
  /* a completer */
}

/* il faut recuperer le numero de page, dire qui est le proprietaire de la page,
envoyer une socket, la recuperer */
static void dsm_handler( void )
{
  /* A modifier */
  printf("[%i] FAULTY  ACCESS !!! \n",DSM_NODE_ID);
  abort();
}

/* traitant de signal adequat */
static void segv_handler(int sig, siginfo_t *info, void *context)
{
  /* A completer */
  /* adresse qui a provoque une erreur */
  void  *addr = info->si_addr;
  /* Si ceci ne fonctionne pas, utiliser a la place :*/
  /*
  #ifdef __x86_64__
  void *addr = (void *)(context->uc_mcontext.gregs[REG_CR2]);
  #elif __i386__
  void *addr = (void *)(context->uc_mcontext.cr2);
  #else
  void  addr = info->si_addr;
  #endif
  */
  /*
  pour plus tard (question ++ savoir si c'est en lecture ou ecriture qui a provoquer le seg fault):
  dsm_access_t access  = (((ucontext_t *)context)->uc_mcontext.gregs[REG_ERR] & 2) ? WRITE_ACCESS : READ_ACCESS;
  */
  /* adresse de la page dont fait partie l'adresse qui a provoque la faute */
  void  *page_addr  = (void *)(((unsigned long) addr) & ~(PAGE_SIZE-1));

  // if pour savoir si c'est un vrai ou un faux seg fault
  if ((addr >= (void *)BASE_ADDR) && (addr < (void *)TOP_ADDR))
  {
    dsm_handler();
  }
  else
  {
    /* SIGSEGV normal : ne rien faire*/
  }
}

/* Seules ces deux dernieres fonctions sont visibles et utilisables */
/* dans les programmes utilisateurs de la DSM                       */
char *dsm_init(int argc, char **argv)
{
  char port_char[30];
  memset(port_char,'\0',30);
  dsm_proc_t *proc_array = NULL;
  fflush(stdout);
  fflush(stderr);
  struct sigaction act;
  int index;
  int port_num;
  int sockfd = atoi(argv[argc-2]);
  int sock=atoi(argv[argc-1]);
  int i;
  int size_host;
  char * buffer;
  int * sockets;
  int *sock_listens;
  int nb_listens;
  int r;
  /* reception du nombre de processus dsm envoye */
  /* par le lanceur de programmes (DSM_NODE_NUM)*/
  r=read(sockfd, &DSM_NODE_NUM, sizeof(int));
  sockets=malloc(DSM_NODE_NUM*sizeof(int));
  /* reception de mon numero de processus dsm envoye */
  /* par le lanceur de programmes (DSM_NODE_ID)*/
  read(sockfd, &DSM_NODE_ID, sizeof(int));
  fflush(stdout);
  fflush(stderr);
  proc_array=malloc(DSM_NODE_NUM*sizeof(dsm_proc_t));
  /* reception des informations de connexion des autres */
  int rang;
  int a;
  for(i=0;i<DSM_NODE_NUM;i++){
    proc_array[i].connect_info.rank=i;
  }
  for(i=0;i<DSM_NODE_NUM;i++){
    read(sockfd,&(rang),sizeof(int));
    a =rang;
    fflush(stdout);
    fflush(stderr);
    read(sockfd,&(proc_array[a].connect_info.port_distant),sizeof(int));
    read(sockfd,&(proc_array[a].connect_info.size_hostname),sizeof(int));
    size_host=proc_array[a].connect_info.size_hostname;
    buffer=malloc(size_host*sizeof(char));
    proc_array[a].connect_info.hostname=malloc(size_host*sizeof(char));
    read(sockfd,buffer,size_host*sizeof(char));
    strcpy(proc_array[a].connect_info.hostname,buffer);
  }
  for(i=0;i<DSM_NODE_NUM;i++){
    fprintf(stderr,"####### je suis le %d element voici mon nom %s et mon port %d et rang %d#######\n",i,proc_array[i].connect_info.hostname,proc_array[i].connect_info.port_distant,proc_array[i].connect_info.rank);
  }
  /* processus envoyees par le lanceur : */
  /* nom de machine, numero de port, etc. */
  fprintf(stderr,"###num_procs: %d & rank: %d & port_num: %d\n", DSM_NODE_NUM, DSM_NODE_ID, port_num);
 //int j;
  //for (j = 0; j<DSM_NODE_NUM-1;j++){

    //for(i=1;i<DSM_NODE_NUM;i++){
      //struct addrinfo hints,*res,*p;
      //int status,sock;
      //memset(&hints, 0, sizeof hints);
      //hints.ai_family = AF_INET;
      //hints.ai_socktype = SOCK_STREAM;
      //hints.ai_flags = AI_PASSIVE;
      //fprintf(stderr,"....salut avant le getaddrinfo %d & %d\n----",DSM_NODE_ID,j);
      //fprintf(stderr,"nom de la machine %s\n",proc_array[i-1].connect_info.hostname);
      //fprintf(stderr,"num du port %d\n",proc_array[i-1].connect_info.port_distant);


      //int port_numm=proc_array[i-1].connect_info.port_distant;

      //if ((status = getaddrinfo(proc_array[i-1].connect_info.hostname, port_numm, &hints, &res)) != 0)
      //fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
      //fflush(stderr);
      //fprintf(stderr,"....salut après le getaddrinfo %d & %d\n----",DSM_NODE_ID,j);
      //for(p = res; p != NULL; p = p->ai_next) {
        //if (connect(sock, p->ai_addr, p->ai_addrlen)!=0)
        //perror("connect dsmwrap");
      //}
      //fprintf(stderr,"....salut après le connect %d & %d\n----",DSM_NODE_ID,j);

      //freeaddrinfo(res);
    //}

    //if (j==DSM_NODE_ID){
      //for(i=0;i<DSM_NODE_NUM-DSM_NODE_ID-1;i++){
        //sleep(2);
        //fprintf(stderr,"....salut %d & %d\n----",DSM_NODE_ID,j);
        //int new_sockfd;
        //char hostname[INET_ADDTSTRLEN];
        //struct sockaddr_in client_addr;
        //socklen_t len = sizeof(client_addr);
        //fprintf(stderr,"....salut avant l'accept %d & %d\n----",DSM_NODE_ID,j);
        //new_sockfd=accept(sock,(struct sockaddr*) &client_addr, &len );
        //fprintf(stderr,"....salut après l'accept %d & %d\n----",DSM_NODE_ID,j);
        //inet_ntop(AF_INET,&(client_addr.sin_addr),hostname,sizeof(hostname));
        //int port_client=htons(client_addr.sin_port);
        //for(i=0;i<DSM_NODE_NUM;i++){
          //if(port_client==proc_array[i].connect_info.port_distant){
            //proc_array[i].connect_info.new_sockfd=new_sockfd;
          //}
        //}
        //proc_array[].connect_info.new_sockfd=
      //}
    //}
  //}
  //fprintf(stderr,"..................connexion aux procs distants..............\n");


  //if(DSM_NODE_ID==1){
    //int a=30;
    //write(proc_array[2].connect_info.new_sockfd, &a,sizeof(int));
  //}
  //if(DSM_NODE_ID==2){
    //int b;
    //read(proc_array[1].connect_info.new_sockfd, &b,sizeof(int));
    //fprintf(stderr,"Ouiii j'ai reçu b %d\n",b);
  //}


  //char* hostname = get_argument(buffer,4)

  /* initialisation des connexions */
  /* avec les autres processus : connect/accept */

  //for(i=0;i<DSM_NODE_NUM-1-DSM_NODE_ID;i++){
  //int portnum;
  //sock_listens[i] = creer_socket(SOCK_STREAM,&portnum);
  //
  //sockets[i]=accept(sockfd,(struct sockaddr*) &client_addr, &len );
  //}

  /* Allocation des pages en tourniquet */
  for(index = 0; index < PAGE_NUMBER; index ++){
    if ((index % DSM_NODE_NUM) == DSM_NODE_ID)
    dsm_alloc_page(index);
    dsm_change_info( index, WRITE, index % DSM_NODE_NUM);
  }

  /* mise en place du traitant de SIGSEGV */
  act.sa_flags = SA_SIGINFO;
  act.sa_sigaction = segv_handler;
  sigaction(SIGSEGV, &act, NULL);

  /* creation du thread de communication */
  /* ce thread va attendre et traiter les requetes */
  /* des autres processus */
  pthread_create(&comm_daemon, NULL, dsm_comm_daemon, NULL);

  /* Adresse de début de la zone de mémoire partagée */
  return ((char *)BASE_ADDR);
}

void dsm_finalize( void )
{
  /* fermer proprement les connexions avec les autres processus */

  /* terminer correctement le thread de communication */
  /* pour le moment, on peut faire (mais ce sera a modifier): */
  pthread_cancel(comm_daemon);

  return;
}
